﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public Character player; // The player object

    public UIManager ui; // The UI script


    private int health = 3; // Player's Health

    private int score = 0; // Player's Score


    EndGameEvent endGameEvent; // Event that occurs at the end of the game



    private void Awake()
    {
        instance = this; // set the static gamemanager instance variable to this instance
        endGameEvent = new EndGameEvent(); //Initialize the endgame event for use later
    }

    public void decreaseHealth() // Decreases the health in the game state and displays the new value to the UI
    {
        health--;
        ui.setHealth(health); //Display the decreased health to the UI

        if (health <= 0) // if the players health reaches 0 the endgame event will activate
        {
            endGameEvent.Invoke(score);
        }
    }
      public void increaseScore() // Increases the score in the game state and displays the new value to the UI
      {
        score++;
        ui.setScore(score); // Display the increased score to the UI
      }

   public void RegisterForEndGameEvent(UnityAction<int> call)
    {
        endGameEvent.AddListener(call);
    }
}
