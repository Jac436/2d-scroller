﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    public int speed = 3;

    protected Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); 
    }

    public void movement(int speed)
    {
        rb.velocity = new Vector2(-speed, 0f);
    }

    public virtual void FixedUpdate()
    {
        movement(speed);
    }

}
