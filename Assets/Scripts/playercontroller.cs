﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playercontroller : MonoBehaviour
{
  public float speed;
  public float acc;
  private Rigidbody2D rb;
  AudioSource audioSource;
  public GameObject Explosion;

    [SerializeField]
  private Vector3 velocity;


  // Start is called before the first frame update
  void Start()
  {
        audioSource = this.gameObject.GetComponent<AudioSource>();
        rb = gameObject.GetComponent<Rigidbody2D>();
  }

  // Update is called once per frame
  void Update()
  {
    if (Input.GetKey(KeyCode.W))
    {
      //move up
      if (rb.velocity.y < speed)
      {
        rb.velocity += new Vector2(0, 1) * acc;
      }
    }
    else if (Input.GetKey(KeyCode.S))
    {
      //move down
      if (rb.velocity.y > -speed)
      {
        rb.velocity += new Vector2(0, 1) * -acc;
      }
    }
    else
    {
      if (rb.velocity.y < -0.05f)
       {
          //if we were going down
          rb.velocity += new Vector2(0, 1) * -acc;
       }
       else if (rb.velocity.y > 0.05f)
       {
         //if we were going up
         rb.velocity += new Vector2(0, 1) * acc;
       }
       else
       {
         rb.velocity = Vector2.zero;
       }
     }
      velocity = rb.velocity;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "token")
        {
            GameManager.instance.increaseScore();
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
            Destroy(collision.gameObject);
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "obstacle")
        {
            GameManager.instance.decreaseHealth();
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
            Destroy(collision.gameObject.transform.parent.gameObject);
        }
    }

    public void DestroyCar(int score)
    {
        Instantiate(Explosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }


}

