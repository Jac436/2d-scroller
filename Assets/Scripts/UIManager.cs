﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text Health; // Text box to display health in UI

    public Text Score; // Text box to display score in UI

    public Text endGamePanel; // Text box to display endgame message in UIo


    // Start is called before the first frame update
    void Start()
    {
        //Register for end game event
        GameManager.instance.RegisterForEndGameEvent(DisplayEndGameMessage);
    }


    // Sets the score to be displayed in the UI
    public void setScore(int newScore) //param "newScore" is the new score to be displayed
    {
        Score.text = "Score: " + newScore;
    }

    // Sets the health to be displayed in the UI
    public void setHealth(int newHealth) //param "newHealth" is the new health to be displayed
    {
        Health.text = "Health: " + newHealth;
    }

    public void DisplayEndGameMessage(int score)
    {
        endGamePanel.text = "Game Over: Your final score was: " + score;
    }

}
