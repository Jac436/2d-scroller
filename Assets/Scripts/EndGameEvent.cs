﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]

public class EndGameEvent : UnityEvent<int>
{
    
}
