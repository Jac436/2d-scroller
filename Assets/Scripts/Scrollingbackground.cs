﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrollingbackground : MonoBehaviour
{

    public GameObject left; // Gameobject that holds the top of the road

    public GameObject right; //Gameobject that holds the top piece of the sprite


    private float endPosition; // the position that will trigger the reset of the plane

    private float spriteWidth; // variable to hold the height of the sprite

    public float movementSpeed; //variable that holds how fast the background will move


    // Start is called before the first frame update
    void Start()
    {
        spriteWidth = right.GetComponentInChildren<SpriteRenderer>().bounds.size.y;
        //Move the bottom sprite to a starting to a starting position below the sprite
        left.transform.position = new Vector2(left.transform.position.x - spriteWidth, left.transform.position.y);
        endPosition = left.transform.position.x;

    }


    // Update is called once per frame
    void Update()
    {
        // Move both pieces at a constant rate
        movePiece(left);
        movePiece(right);

        // If either of the two pieces passes the intial postion of the left piece reset it
        if (left.transform.position.x <= endPosition)
        {
            resetPiece(left);
        }
        if (right.transform.position.x <= endPosition)
        {
            resetPiece(right);
        }
    }

    void resetPiece(GameObject piece)
    {
        // move the piece upward by the length of 2 pieces
        piece.transform.position = new Vector2(piece.transform.position.x + spriteWidth * 2, piece.transform.position.y);
    }
    void movePiece(GameObject piece)
    {
        // Move the piece at a constant speed
        piece.transform.Translate(-transform.right * movementSpeed * Time.deltaTime);
    }
}