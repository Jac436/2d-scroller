﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
  public static float lastSpawn = 0; // Last spawn time

  public GameObject obstacle; // obstacle prefab

  public GameObject token; //token prefab

  private static float spawnInterval = 3f; // Minimum time between spawns

  private static Spawner lastSpawnerUsed; // Variable for the last spawner that spawned an object

  // Start is called before the first frame update
  void Start()
  {
    lastSpawn = Random.Range(0, 5);
    lastSpawnerUsed = this;
  }

  // Update is called once per frame
  void Update()
  {
    objectSpawn(); //Attempt to spawn an object  
  }



  void objectSpawn()
  {
    //If the spawn interval has passed
    if((Time.fixedTime - lastSpawn)> spawnInterval && lastSpawnerUsed.Equals(this))
    {
      if(Random.Range(0, 100) < 50) // Creates a roughly 20% chance of spawning an obstacle
      {
        // Spawn an obstacle at the spawner's location
        Instantiate(obstacle, new Vector2(transform.position.x, Random.Range(-5.0f, 5.0f)), Quaternion.Euler(0, 0, -90));
        // Set the now most recent spawn time
        lastSpawn = Time.fixedTime;
        lastSpawnerUsed = this;
      }
      else if (Random.Range(0, 100) < 70) //Creates a roughly 5 percent chance a token will spawn
      {
        //Spawn a token at the spawner's location
        Instantiate(token, new Vector2(transform.position.x, Random.Range(-5.0f, 5.0f)), Quaternion.identity);
        // Set the now most recent spawn time
        lastSpawn = Time.fixedTime;
        lastSpawnerUsed = this;
      }
    }
  }
    
}
