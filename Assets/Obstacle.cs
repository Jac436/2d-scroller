﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MovingObject
{
    public GameObject Explosion; //The explosion prefabto instantiate when the car collides with the player

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }


    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Player")
        {
            // Instantiate an explosion over top of the obstacle
            Instantiate(Explosion, transform.position, transform.rotation);

            // Decrease health in the game state
            GameManager.instance.decreaseHealth();
            // Destroy this obstacle
            Destroy(gameObject);
        }
    }
}
